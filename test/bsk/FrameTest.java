package bsk;

import static org.junit.Assert.*;

import org.junit.Test;
import tdd.training.bsk.*;

public class FrameTest {

	@Test
	public void testGetFirstThrow(){
		Frame frame = new Frame(2,4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow(){
		Frame frame = new Frame(2,4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testFrameScore(){
		Frame frame = new Frame(2,6);
		assertEquals(8, frame.getScore());
	}

}
