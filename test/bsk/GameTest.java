package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.*;

public class GameTest {

	@Test
	public void gameShouldBeCreateEmpty() {
		Game game = new Game();
	
		assertEquals(0, game.getFrameAt(9).getFirstThrow());
	}
	
	@Test
	public void gameShouldBeCreateWithValues() {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(2, game.getFrameAt(9).getFirstThrow());
	}

	@Test
	public void gameScoreShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void gameSpareShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void gameStrikeShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void gameStrikeAndSpareShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void gameMultipleStrikeShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void gameMultipleSpareShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void gameSpareLastFrameShouldBeCalculate(){
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void gameStrikeLastFrameShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void gameBestScoreShouldBeCalculate() {
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}

}
