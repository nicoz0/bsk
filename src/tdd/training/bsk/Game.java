package tdd.training.bsk;

public class Game {
	
	Frame[] bowlingGame = new Frame[10];
	private int numberFrame=0;
	private int totalScore=0;
	private int firstBonus=0;
	private int secondBonus=0;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		for(int i=0; i< bowlingGame.length; i++) {
			bowlingGame[i] = new Frame(0,0);
		}
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
		
		bowlingGame[numberFrame] = new Frame(frame.getFirstThrow(),frame.getSecondThrow());
		numberFrame++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		// To be implemented
		return bowlingGame[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		firstBonus=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) {
		secondBonus=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return firstBonus;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return secondBonus;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() {
		
		for(int i=0; i<bowlingGame.length; i++) {
			if(bowlingGame[i].isSpare()) {
				isSpareCalculateScore(i);
			}else {
				if(bowlingGame[i].isStrike()) {
					
					isStrikeCalculateScore(i);
				}else {
					totalScore=totalScore+bowlingGame[i].getScore();
				}
			}
		}
		
		return totalScore;	
		
	}

	private void isStrikeCalculateScore(int i) {
		if(i==9) {
			totalScore=totalScore+bowlingGame[i].getScore()+getFirstBonusThrow()+getSecondBonusThrow();
			
		}else {
			bowlingGame[i].setBonus(bowlingGame[i+1].getFirstThrow()+bowlingGame[i+1].getSecondThrow());
			totalScore+=bowlingGame[i].getScore();
			
			if(bowlingGame[i+1].isStrike()) {
				if(i!=8) {
					totalScore+=bowlingGame[i+2].getFirstThrow();
				}else {
					totalScore+=bowlingGame[i+1].getFirstThrow();
				}	
			}
		}
	}

	private void isSpareCalculateScore(int i) {
		

		if(i==9) {
			totalScore=totalScore+bowlingGame[i].getScore()+getFirstBonusThrow();
		}else {
			bowlingGame[i].setBonus(bowlingGame[i+1].getFirstThrow());
			totalScore=totalScore+bowlingGame[i].getScore();
		}
		
	}

}
